package home7;

import java.util.Hashtable;
import java.util.LinkedList;
import java.util.Map;

public class Puzzle {
	private static String text;
	private static boolean end;
	private static int solutions;
	private static String[] words_list;
	private static String[] letters_list;
	private static String[] not_repeated_letters;
	private static LinkedList<Integer> used_numbers;
	private static Map<String, Integer> letters_dict;

	/**
	 * Solve the word puzzle.
	 * 
	 * @param args
	 *            three words (addend1, addend2 and sum)
	 */
   public static void main (String[] args) {
		Puzzle p = new Puzzle();
		Puzzle.get_user_input(args);
	}

	public Puzzle() {
		text = "";
		end = false;
		solutions = 0;
		words_list = null;
		letters_list = null;
		not_repeated_letters = null;
		used_numbers = new LinkedList<Integer>();
		letters_dict = new Hashtable<String, Integer>();
	}

	public static void get_user_input(String[] args) {

		/* SCANNER
		System.out.println("please give three words (addend1, addend2 and sum)");
		System.out.print(">");
		Scanner read = new Scanner(System.in);
		text = read.nextLine();
		*/

		for (String i : args) {
			text += i + " ";
		}

		// delete all extra white space
		text = text.replaceAll("\\s+", " ");
		if (text.charAt(0) == ' ') {
			text = text.substring(1);
		}

		// make a list that contains 3 word.
		words_list = text.split(" ");
		for (String i : words_list) {
			if (i.length() > 18)
				throw new RuntimeException("A single word can not be bigger than 18 letters. Error at:" + i);
		}

		// make a list that contains all letters.
		letters_list = text.replaceAll(" ", "").split("");

		// make a dictionary that contains letters(non repeated) and there cost.
		int n = 0;
		for (String i : letters_list) {
			if (letters_dict.get(i) == null) {
				letters_dict.put(i, -1);
			}
		}

		// make a list containing numbers from 0 to 9.
		for (int i = 0; i < 10; i++) {
			used_numbers.add(i);
		}

		// make a list that contains non repeated letters.
		not_repeated_letters = new String[letters_dict.size()];
		n = 0;
		for (String i : letters_dict.keySet()) {
			not_repeated_letters[n] = i;
			n++;
		}

		//start searching
		System.out.println("Searching for solutions");
		solve(0);
		if (!end) {
			System.out.println("There are no solutions.");
		} else {
			System.out.println("There is a tottal of: " + solutions + " solutions(may contain duplicate solutions)");
			// System.out.println("There is atleast 1 solution");
		}
	}
	
	// takes a word and based on the dictionary returns a number
	public static Long get_number(String[] word) {
		String number="";
		for (String i : word) {
			number += letters_dict.get(i);
		}
		if (number.charAt(0) == '0') {
			return (long) -1;
		} else {
			return Long.valueOf(number);
		}
	}

	// check the solution.
	public static void check() {
		long number_x1 = get_number(words_list[0].split(""));
		long number_x2 = get_number(words_list[1].split(""));
		long number_y = get_number(words_list[2].split(""));
		if (number_x1 != -1 && number_x2 != -1 && number_y != -1) {
			if (number_x1 + number_x2 == number_y) {
				solutions++;
				if (!end) {
					System.out.println(words_list[0] + " + " + words_list[1] + " = " + words_list[2]);
					System.out.println(number_x1 + " + " + number_x2 + " = " + number_y);
				}
				end = true;
			}
		}
	}

	// main program.
	public static void solve(int n) {
		int list_size = used_numbers.size();
		for (int i = 0; i < list_size; i++) {
			letters_dict.put(not_repeated_letters[n], used_numbers.poll());
			if (n == letters_dict.size() - 1) {
				check();
			}

			// remove is want to find all solutions.
			//if (end) {
			//	break;
			//}


			if(n+1<letters_dict.size()){
				solve(++n);
				n--;
			}
			if (letters_dict.get(not_repeated_letters[n]) != -1) {
				used_numbers.add(letters_dict.get(not_repeated_letters[n]));
			}
		}
		letters_dict.put(not_repeated_letters[n], -1);
	}
}