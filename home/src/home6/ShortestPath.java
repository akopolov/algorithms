package home6;

import java.util.LinkedList;

public class ShortestPath {
	private Graph G;
	private static LinkedList<String> pathList = new LinkedList<String>();

	public static class Node {
		public Node[] edges;
		public String name;
		public boolean visited;

		public Node(String name) {
			this.name = name;
			this.visited = false;
		}

		public String GetName() {
			return name;
		}

		public Node[] GetEdges() {
			return edges;
		}

		public boolean GetVisited() {
			return visited;
		}
	}

	public static class Graph {
		public Node[] graph;

		public Graph() {

		}

		public Node[] GetElements() {
			return graph;
		}
	}

	public static void main(String[] args) {
		Node A = new Node("A");
		Node B = new Node("B");
		Node C = new Node("C");
		Node D = new Node("D");
		Node F = new Node("F");
		Node H = new Node("H");
		Node J = new Node("J");
		A.edges = new Node[] { B, C, D, J };
		B.edges = new Node[] { C, F };
		F.edges = new Node[] { D };
		C.edges = new Node[] { H };
		H.edges = new Node[] { D };
		D.edges = new Node[] { C };
		J.edges = new Node[] { B };
		Graph G = new Graph();
		G.graph = new Node[] { A, B, C, D, F, H, J };
		ShortestPath S = new ShortestPath(G);
		S.AllPaths();
		
		Node L = new Node("L");
		Node I = new Node("I");
		Node O = new Node("O");

		L.edges = new Node[] { I, O };
		I.edges = new Node[] { L, O };
		O.edges = new Node[] { I, L };
		G.graph = new Node[] { I, L, O };
		S = new ShortestPath(G);
		S.AllPaths();
	}

	public ShortestPath(Graph G) {
		this.G = G;
	}

	public String Paths(Node N, String path) {
		boolean moveBack = false;
		N.visited = true;
		path += N.name + "-";
		for (Node T : N.edges) {
			if (!T.visited) {
				path = Paths(T, path);
				moveBack = true;
			}
		}
		if (moveBack) {
			path = path.substring(0, path.length() - 2);
		} else {
			if (!path.equals(" ")) {
				pathList.add(path.substring(0, path.length() - 1));
			}
			path = path.substring(0, path.length() - 2);
		}
		N.visited = false;
		return path;
	}

	public void AllPaths() {
		pathList = new LinkedList<String>();
		LinkedList<Node> visited = new LinkedList<Node>();
		for (Node N : this.G.GetElements()) {
			if (!visited.contains(N)) {
				Node A = N;
				visited.add(N);
				Paths(A, "");
			}
		}
		for (String s : pathList) {
			System.out.println(s);
		}
	}
}
