package home6;

import java.util.Random;

/** Container class to different classes, that makes the whole
 * set of classes one class formally.
 */
public class GraphTask {

   /** Main method. */
   public static void main (String[] args) {
		GraphTask a = new GraphTask();
		a.run();
   }

   /** Actual main method to run examples and everything. */
	public void run() {
      Graph g = new Graph ("G");
		g.createRandomSimpleGraph(6, 5);
		g.shortestPath();
      // TODO!!! Your experiments here
   }

   class Vertex {
      private String id;
      private Vertex next;
      private Arc first;
      private int info = 0;
      // You can add more fields, if needed

      Vertex (String s, Vertex v, Arc e) {
         id = s;
         next = v;
         first = e;
      }

      Vertex (String s) {
         this (s, null, null);
      }

      @Override
      public String toString() {
         return id;
      }
   }


   /** Arc represents one arrow in the graph. Two-directional edges are
    * represented by two Arc objects (for both directions).
    */
   class Arc {
      private String id;
      private Vertex target;
      private Arc next;
		// TODO!!! Add weight to arc
		private int distance;
		// You can add more fields, if needed

		Arc(String s, Vertex v, Arc a, int d) {
         id = s;
         target = v;
         next = a;
			distance = d;
			// distance = d;
			// TODO!!! Generate a random number for tests
			// Random r = new Random();
			// distance = r.nextInt((30 - 1) + 1) + 1;
      }

      Arc (String s) {
			this(s, null, null, 0);
      }

      @Override
      public String toString() {
         return id;
      }
   } 


   class Graph {
      private String id;
      private Vertex first;
      private int info = 0;
      // You can add more fields, if needed

      Graph (String s, Vertex v) {
         id = s;
         first = v;
      }

      Graph (String s) {
         this (s, null);
      }

      @Override
      public String toString() {
         String nl = System.getProperty ("line.separator");
         StringBuffer sb = new StringBuffer (nl);
         sb.append (id);
         sb.append (nl);
         Vertex v = first;
         while (v != null) {
            sb.append (v.toString());
            sb.append (" -->");
            Arc a = v.first;
            while (a != null) {
               sb.append (" ");
               sb.append (a.toString());
               sb.append (" (");
               sb.append (v.toString());
               sb.append ("->");
               sb.append (a.target.toString());
               sb.append (")");
               a = a.next;
            }
            sb.append (nl);
            v = v.next;
         }
         return sb.toString();
      }

      public Vertex createVertex (String vid) {
         Vertex res = new Vertex (vid);
         res.next = first;
         first = res;
         return res;
      }

		public Arc createArc(String aid, Vertex from, Vertex to, int cost) {
         Arc res = new Arc (aid);
         res.next = from.first;
         from.first = res;
         res.target = to;
			res.distance = cost;
         return res;
      }

      /**
       * Create a connected undirected random tree with n vertices.
       * Each new vertex is connected to some random existing vertex.
       * @param n number of vertices added to this graph
       */
      public void createRandomTree (int n) {
         if (n <= 0)
            return;
         Vertex[] varray = new Vertex [n];
         for (int i = 0; i < n; i++) {
            varray [i] = createVertex ("v" + String.valueOf(n-i));
            if (i > 0) {
               int vnr = (int)(Math.random()*i);
               Random r = new Random();
               createArc ("a" + varray [vnr].toString() + "_"
							+ varray[i].toString(), varray[vnr], varray[i], r.nextInt((30 - 1) + 1) + 1);
               createArc ("a" + varray [i].toString() + "_"
							+ varray[vnr].toString(), varray[i], varray[vnr], r.nextInt((30 - 1) + 1) + 1);
            } else {}
         }
      }

      /**
       * Create an adjacency matrix of this graph.
       * Side effect: corrupts info fields in the graph
       * @return adjacency matrix
       */
      public int[][] createAdjMatrix() {
         info = 0;
         Vertex v = first;
         while (v != null) {
            v.info = info++;
            v = v.next;
         }
         int[][] res = new int [info][info];
         v = first;
         while (v != null) {
            int i = v.info;
            Arc a = v.first;
            while (a != null) {
               int j = a.target.info;
               res [i][j]++;
               a = a.next;
            }
            v = v.next;
         }
         return res;
      }

      /**
       * Create a connected simple (undirected, no loops, no multiple
       * arcs) random graph with n vertices and m edges.
       * @param n number of vertices
       * @param m number of edges
       */
      public void createRandomSimpleGraph (int n, int m) {
         if (n <= 0)
            return;
         if (n > 2500)
            throw new IllegalArgumentException ("Too many vertices: " + n);
         if (m < n-1 || m > n*(n-1)/2)
            throw new IllegalArgumentException 
               ("Impossible number of edges: " + m);
         first = null;
         createRandomTree (n);       // n-1 edges created here
         Vertex[] vert = new Vertex [n];
         Vertex v = first;
         int c = 0;
         while (v != null) {
            vert[c++] = v;
            v = v.next;
         }
         int[][] connected = createAdjMatrix();
         int edgeCount = m - n + 1;  // remaining edges
         while (edgeCount > 0) {
            int i = (int)(Math.random()*n);  // random source
            int j = (int)(Math.random()*n);  // random target
            if (i==j) 
               continue;  // no loops
            if (connected [i][j] != 0 || connected [j][i] != 0) 
               continue;  // no multiple edges
            Vertex vi = vert [i];
            Vertex vj = vert [j];
				Random r = new Random();
				createArc("a" + vi.toString() + "_" + vj.toString(), vi, vj, r.nextInt((30 - 1) + 1) + 1);
            connected [i][j] = 1;
				createArc("a" + vj.toString() + "_" + vi.toString(), vj, vi, r.nextInt((30 - 1) + 1) + 1);
            connected [j][i] = 1;
            edgeCount--;  // a new edge happily created
         }
      }

		// TODO!!! Your Graph methods here! Probably your solution belongs here.

		/**
		 * Generate a ARC matrix.
		 * @param len length of adjmatrix;
		 * @param v the start Vertex of a graph;
		 */
		public int[][] buildArcMatrix(int len, Vertex v) {
			int[][] Amatrix = new int[len][len];
			while (v != null) {
				Arc f = v.first;
				while (f != null) {
					if (f.distance < 0) {
						throw new RuntimeException("A path cost can not be negetive problem at: " + f.id);
					} else {
						Amatrix[v.info][f.target.info] = f.distance;
						f = f.next;
					}
				}
				v = v.next;
			}
			for (int i = 0; i < Amatrix.length; i++) {
				for (int j = 0; j < Amatrix.length; j++) {
					if (Amatrix[i][j] == 0 && i != j) {
						Amatrix[i][j] = Integer.MAX_VALUE;
					}
				}
			}
			return Amatrix;
		}

		/** Implementing FloydWarshall algorithm 
		 *@param matrix adjmatrix*/		
		public void floydWarshall(int[][] matrix) {
			for (int k = 0; k < matrix.length; k++) {
				for (int i = 0; i < matrix.length; i++) {
					for (int j = 0; j < matrix.length; j++) {
						if (matrix[i][k] < Integer.MAX_VALUE && matrix[k][j] < Integer.MAX_VALUE) {
							if (matrix[i][k] + matrix[k][j] < matrix[i][j]) {
								matrix[i][j] = matrix[i][k] + matrix[k][j];
							}
						}
					}
				}
			}
		}

		/** Implementing Dijkstra algorithm 
		 *@param Amatrix a adjmatrix
		 *@param start from adjmatrix select the main vertex
		 *@param paths a list that contains all the paths from START to any other vertex
		 *@param visited a list that contains all the vertexes */
		public void dijkstra(int[][] Amatrix, int start, int[][] paths, boolean[] visited) {
			visited[start] = true;
			int[] next = {Integer.MAX_VALUE,Integer.MAX_VALUE};
			for (int i = 0; i < Amatrix.length; i++) {
				if (Amatrix[start][i] != Integer.MAX_VALUE) {
					if (Amatrix[start][i] + paths[start][0] < paths[i][0] && !visited[i]) {
						paths[i][0] = Amatrix[start][i] + paths[start][0];
						paths[i][1] = start;
					}
				}
			}
			for (int i = 0; i < paths.length; i++) {
				if (!visited[i]) {
					if (paths[i][0] < next[1]) {
						next[1] = paths[i][0];
						next[0] = i;
					}
				}
			}
			if (next[0] != Integer.MAX_VALUE) {
				dijkstra(Amatrix, next[0], paths, visited);
			}
		}

		/** Returns a string that shows the path 
		 * @param paths a list that contains all the paths from 1 vertex to all others
		 * @param start a number that tells what is the starting vertex
		 * @param end a number that tells what is the ending vertex
		 * @return text a string that shows how to get from START to END*/
		public String getPath(int[][] paths, int start, int end) {
			String text = String.valueOf(end);
			while (start != end) {
				text = String.valueOf(paths[end][1]) + "->" + text;
				end = paths[end][1];
			}
			return text;
		}

		/** Check if the given graph is empty 
		 * @param graph takes a object(Graph)*/
		public void check(Graph graph) {
			if (graph.first == null) {
				throw new RuntimeException("the graph:" + graph.id + " is empty");
			}
		}

		/** builds a string that the user will receive.
		 * @param pathlist a list that contains paths(1,3,4,0)
		 * @param v a Vertex that is used to find the names of the Vertexes 
		 */
		public String[] buildpath(String[] pathlist, Vertex v) {
			int n = 0;
			String[] path = new String[pathlist.length];
			while (v != null) {
				for (int i = 0; i < pathlist.length; i++) {
					if (Integer.parseInt(pathlist[i]) == n) {
						path[i] = v.id;
						break;
					}
				}
				// a small bug was here
				n++;
				v = v.next;
			}
			return path;
		}

		/** Main program 
		 * @return a string that shows the shortest path of a longest path in a given graph*/
		public String shortestPath() {
			check(this);
			int[][] shortMatrix = this.createAdjMatrix();
			int[][] Amatrix = this.createAdjMatrix();
			int[] longPath = { 0, 0, 0 };			
			shortMatrix = buildArcMatrix(shortMatrix.length, this.first);
			Amatrix = buildArcMatrix(shortMatrix.length, this.first);

			/** Generate a boolean list to keep track of visited locations. */
			boolean[] visited = new boolean[Amatrix.length];
			for (int i = 0; i < visited.length; i++) {
				visited[i] = false;
			}

			/** print ARC matrix for tests */
			System.out.println("Normal matrix");
			for (int i = 0; i < Amatrix.length; i++) {
				for (int j = 0; j < Amatrix[i].length; j++) {
					System.out.print(Amatrix[i][j] + "\t");
				}
				System.out.println();
			}

			/** Generate a matrix with the shortest path costs. */
			floydWarshall(shortMatrix);

			/** Find a longest path list[3] where: START,END,COST. */
			System.out.println("\nFloydWarshall matrix:");
			for (int i = 0; i < shortMatrix.length; i++) {
				for (int j = 0; j < shortMatrix[i].length; j++) {
					if (shortMatrix[i][j] > longPath[2] && i != j) {
						longPath[0] = i;
						longPath[1] = j;
						longPath[2] = shortMatrix[i][j];
					}
					System.out.print(shortMatrix[i][j] + "\t");
				}
				System.out.println();
			}

			System.out.println("\nThe longest path is:");
			System.out.println("FROM:" + longPath[0] + " TO:" + longPath[1] + " COST:" + longPath[2] + "\n");

			/**
			 * Generate a matrix[n][2] where n is the length of adjmatrix where:
			 * COST,Previous VERTEX.
			 */
			int[][] paths = new int[Amatrix.length][2];
			for (int i = 0; i < paths.length; i++) {
				if (i != longPath[0]) {
					paths[i][0] = Integer.MAX_VALUE;
				} else {
					paths[i][0] = 0;
				}
			}

			/** Find the path from A to B */
			System.out.println("Dijkstra matrix:");
			dijkstra(Amatrix, longPath[0], paths, visited);
			for (int i = 0; i < paths.length; i++) {
				System.out.println(
						"FROM:" + longPath[0] + " TO:" + i + " COST:" + paths[i][0] + " Previous:" + paths[i][1]);
			}

			/** Dijkstra and FloydWarshall comparison */
			for (int i = 0; i < shortMatrix[longPath[0]].length; i++) {
				if (shortMatrix[longPath[0]][i] != paths[i][0]) {
					throw new RuntimeException(
							"Dijkstra:" + paths[i][0] + " FloydWarshall:" + shortMatrix[longPath[0]][i]
									+ " results do not add up");
				}
			}

			/** Print the path from A to B */
			String path = getPath(paths, longPath[0], longPath[1]);

			/** Using the path find cost */
			int cost = 0;
			String[] pathlist = path.split("->");
			for (int i=0;i<pathlist.length;i++) {
				if (i + 1 != pathlist.length) {
					cost += Amatrix[Integer.parseInt(pathlist[i])][Integer.parseInt(pathlist[i + 1])];
				}
			}

			/**
			 * check that the given path cost is the same as the cost found by
			 * FloydWarshall
			 */
			if (cost != longPath[2]) {
				throw new RuntimeException(
						"The given path:" + path + " cost:" + cost + " is not the same as FloydWarshall cost:"
								+ longPath[2]);
			}

			System.out.println("\n" + "The path(based on adjmatrix):" + path + " and the cost:" + cost + "\n");

			path = "";
			pathlist = buildpath(pathlist, this.first);
			
			for (int i=0;i<pathlist.length;i++) {
				if(i+1!=pathlist.length){
					path += pathlist[i] + "->";
				}else{
					path += pathlist[i];
				}
			}

			System.out.println("The path:" + path + " and the cost:" + cost + "\n");

			System.out.println("______________________________NEXT________________________");

			return path;
		}
	}
} 

