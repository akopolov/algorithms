package home6;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import home6.GraphTask.Graph;
import home6.GraphTask.Vertex;

/** Testklass.
 * @author jaanus
 */
public class GraphTaskTest {

	@Test(timeout = 1000)
	public void testempty() {
		GraphTask gt = new GraphTask();
		Graph g = gt.new Graph("G");
		g.shortestPath();
	}

	@Test(timeout = 1000)
	public void test1(){
		GraphTask gt = new GraphTask();
		Vertex v = gt.new Vertex("v0");
		Graph g = gt.new Graph("G", v);
		String path = g.shortestPath();
		assertEquals("v0", path);
	}

	@Test(timeout = 1000)
	public void test2() {
		GraphTask gt = new GraphTask();
		Graph g = gt.new Graph("G");
		Vertex v2 = g.createVertex("v2");
		Vertex v1 = g.createVertex("v1");
		Vertex v0 = g.createVertex("v0");
		g.createArc("v1-v0", v1, v0, 8);
		g.createArc("v0-v1", v0, v1, 10);
		g.createArc("v0-v2", v0, v2, 4);
		g.createArc("v2-v0", v2, v0, 1);
		g.createArc("v2-v1", v2, v1, 3);

		String path = g.shortestPath();
		assertEquals("v1->v0->v2", path);
	}

	@Test(timeout = 1000)
	public void testnegetiv() {
		GraphTask gt = new GraphTask();
		Graph g = gt.new Graph("G");
		Vertex v0 = g.createVertex("v0");
		Vertex v1 = g.createVertex("v1");
		g.createArc("v1-v0", v1, v0, 8);
		g.createArc("v0-v1", v0, v1, -10);
		String path = g.shortestPath();
	}

	@Test(timeout = 1000)
	public void test3() {
		GraphTask gt = new GraphTask();
		Graph g = gt.new Graph("G");
		Vertex v3 = g.createVertex("v3");
		Vertex v2 = g.createVertex("v2");
		Vertex v1 = g.createVertex("v1");
		Vertex v0 = g.createVertex("v0");
		g.createArc("v1-v0", v1, v0, 8);
		g.createArc("v0-v1", v0, v1, 10);
		g.createArc("v0-v2", v0, v2, 4);
		g.createArc("v2-v0", v2, v0, 1);
		g.createArc("v2-v1", v2, v1, 3);
		g.createArc("v1-v2", v1, v2, 5);
		g.createArc("v2-v3", v2, v3, 6);
		g.createArc("v3-v2", v3, v2, 4);
		String path = g.shortestPath();
		assertEquals("v1->v0->v2->v3", path);
	}
}