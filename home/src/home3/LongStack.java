package home3;

import java.util.Arrays;
public class LongStack {
	private Node first;

	public static void main(String[] argum) {
		System.out.println(interpret("2 -5"));
	}

	public class Node {
		private long item;
		private Node next;

		public Node(long item) {
			this.item = item;
		}
	}

	LongStack() {
		first = null;
	}

	@Override
	public Object clone() throws CloneNotSupportedException {
		LongStack new_long = new LongStack();
		new_long.first = first;
		return new_long;
	}

	public boolean stEmpty() {
		return first == null;
	}

	public void push(long a) {
		Node newfirst = new Node(a);
		newfirst.next = first;
		first = newfirst;
	}

	public long pop() {
		Node temp = first;
		if (stEmpty()) {
			throw new RuntimeException("Stack is Empty");
		}
		first = temp.next;
		return temp.item;
	}// pop

	public void op(String s) {
		long first_long = 0;
		long second_long = 0;
		try{
			first_long = pop();
			second_long = pop();
		}catch(Exception e){
			throw new RuntimeException("Stack is missing 1 or bouth values");
		}
		switch (s) {
		case "+":
			push(first_long + second_long);
			break;
		case "-":
			push(second_long - first_long);
			break;
		case "*":
			push(first_long * second_long);
			break;
		case "/":
			push(second_long / first_long);
			break;
		default:
			throw new RuntimeException("Simbol is missing " + s);
		}
	}
  
   public long tos() {
		return first.item;
   }

   @Override
   public boolean equals (Object o) {
		Node temp1 = first;
		LongStack stack = (LongStack) o;
		Node temp2 = stack.first;
		while (temp1 != null || temp2 != null) {
			try {
				if (temp1.item == temp2.item) {
					temp1 = temp1.next;
					temp2 = temp2.next;
				} else {
					return false;
				}
			} catch (Exception e) {
				return false;
			}
		}
		return true;
   }

   @Override
   public String toString() {
		Node temp = first;
		String text = "", temp_text = "";
		while (temp != null) {
			temp_text = Long.toString(temp.item);
			text = temp_text + text;
			temp = temp.next;
		}
		return text;
   }

	// Checks if a string a is numeric
	public static boolean isNumeric(String str) {
		try {
			long d = Long.parseLong(str);
		} catch (NumberFormatException nfe) {
			return false;
		}
		return true;
	}

   public static long interpret (String pol) {
		LongStack stack = new LongStack();
		String[] math_sumb = { "+", "-", "*", "/" };
		pol = pol.trim();
		if (pol.isEmpty()) {
			throw new RuntimeException("The given string " + pol + " is Empty or contains no values");
		} else {
			for (String item : pol.split("\\s+")) {
				if (isNumeric(item)) {
					long long_item = Long.parseLong(item);
					stack.push(long_item);
				} else {
					if (Arrays.asList(math_sumb).contains(item)) {
						try{
							stack.op(item);
						} catch (Exception e) {
							throw new RuntimeException("Missing some elements " + pol);
						}
					} else if (item != null && item.length() > 0) {
						throw new RuntimeException(
								"The given string " + pol + " contains a non acceptable element : " + item);
					}
				}
			}
		}
		if (stack.first.next != null) {
			throw new RuntimeException(
					"The given string " + pol
							+ " has too many values/mathematical operations OR a incorrect order of values and operations");
		}
		return stack.pop();
   }
}