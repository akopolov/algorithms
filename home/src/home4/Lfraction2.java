package home4;

public class Lfraction2 {

	public Fraction fraction;

	public class Fraction {
		public long numerator;
		public long denominator;

		public Fraction(long n, long d) {
			numerator = n;
			denominator = d;
		}
	}

	public Lfraction2(long numerator, long denominator) {
		fraction = null;
		if (denominator == 0) {
			throw new RuntimeException("Denominator is zero");
		} else if (denominator < 0) {
			numerator*=-1;
			denominator*=-1;
			fraction = new Fraction(numerator, denominator);
		}else{
			fraction = new Fraction(numerator, denominator);
		}
	}

	public long getNumerator() {
		return fraction.numerator;
	}

	public long getDenominator() {
		return fraction.denominator;
	}

	@Override
	public String toString() {
		return fraction.numerator + "/" + fraction.denominator;
	}

	@Override
	public boolean equals(Object o) {
		Lfraction2 new_Lf = (Lfraction2) o;
		Fraction new_fr = new_Lf.fraction;
		if (fraction.numerator == new_fr.numerator && fraction.denominator == new_fr.denominator) {
			return true;
		}
		return false;
	}

	@Override
	public int hashCode() {
		/*int prime = 31;
		int result = 1;
		result = prime * result + (int) fraction.numerator;
		result = prime * result + (int) fraction.denominator;
		*/
		return (int) ((int) fraction.numerator * 31 + (int) fraction.denominator);
	}
}
