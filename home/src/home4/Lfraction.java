package home4;

/** This class represents fractions of form n/d where n and d are long integer 
 * numbers. Basic operations and arithmetics for fractions are provided.
 */
public class Lfraction implements Comparable<Lfraction> {
	public long numerator;
	public long denominator;

   /** Main method. Different tests. */
   public static void main (String[] param) {
		System.out.println(valueOf("-4/-0"));
   }

   /** Constructor.
    * @param a numerator
    * @param b denominator > 0
    */
   public Lfraction (long a, long b) {
		if (b == 0) {
			throw new RuntimeException("Denominator is zero");
		} else if (b < 0) {
			a *= -1;
			b *= -1;
		}
		long gcdivisor = gcm(a, b);
		this.numerator = a / gcdivisor;
		this.denominator = b / gcdivisor;
   }

	/** Takes 2 longs and returns there greatest common divisor */
	public static long gcm(long a, long b) {
		if (b == 0) {
			return Math.abs(a);
		}
		return gcm(b, a % b);
	}

	/** minimizes a fraction */
	public void minimize() {
		long gcdivisor = gcm(this.numerator, this.denominator);
		this.denominator /= gcdivisor;
		this.numerator /= gcdivisor;
	}

   /** Public method to access the numerator field. 
    * @return numerator
    */
   public long getNumerator() {
      return this.numerator;
   }

   /** Public method to access the denominator field. 
    * @return denominator
    */
   public long getDenominator() { 
      return this.denominator;
   }

   /** Conversion to string.
    * @return string representation of the fraction
    */
   @Override
   public String toString() {
      return this.numerator+"/"+this.denominator;
   }

   /** Equality test.
    * @param m second fraction
    * @return true if fractions this and m are equal
    */
   @Override
   public boolean equals (Object m) {
	   Lfraction new_fraction = (Lfraction) m;
	   new_fraction.minimize();
	   return (new_fraction.numerator == this.numerator && new_fraction.denominator == this.denominator);
   }

   /** Hashcode has to be equal for equal fractions.
    * @return hashcode
    */
   @Override
   public int hashCode() {
		/** method #1 */
		int prime = 31;
		int result = 1;
		result = prime * result + (int) numerator;
		result = prime * result + (int) denominator;
		return result;

	   /** method #2
		String result = "";
		result += this.numerator;
		result += this.denominator;
		return Integer.parseInt(result);
		*/
   }

   /** Sum of fractions.
    * @param m second addend
    * @return this+m
    */
   public Lfraction plus (Lfraction m) {
		long new_numerator = (this.numerator * m.denominator + m.numerator * this.denominator);
		long new_denominator = this.denominator * m.denominator;
		return new Lfraction(new_numerator, new_denominator);
   }

   /** Multiplication of fractions.
    * @param m second factor
    * @return this*m
    */
   public Lfraction times (Lfraction m) {
		long new_numerator = this.numerator * m.numerator;
		long new_denominator = this.denominator * m.denominator;
		return new Lfraction(new_numerator, new_denominator);
   }

   /** Inverse of the fraction. n/d becomes d/n.
    * @return inverse of this fraction: 1/this
    */
   public Lfraction inverse() {
		if (this.numerator == 0) {
			throw new RuntimeException("Numerator is zero and can not be Inverted");
		}
		return new Lfraction(this.denominator, this.numerator);
   }

   /** Opposite of the fraction. n/d becomes -n/d.
    * @return opposite of this fraction: -this
    */
   public Lfraction opposite() {
		return new Lfraction(this.numerator * -1, this.denominator);
   }

   /** Difference of fractions.
    * @param m subtrahend
    * @return this-m
    */
   public Lfraction minus (Lfraction m) {
		long new_numerator = (this.numerator * m.denominator - m.numerator * this.denominator);
		long new_denominator = this.denominator * m.denominator;
		return new Lfraction(new_numerator, new_denominator);
   }

   /** Quotient of fractions.
    * @param m divisor
    * @return this/m
    */
   public Lfraction divideBy (Lfraction m) {
	   if(m.numerator==0){
			throw new RuntimeException("The second fraction numerator is zero -> Fraction/0");
		}
		long new_numerator = this.numerator * m.denominator;
		long new_denominator = this.denominator * m.numerator;
		return new Lfraction(new_numerator, new_denominator);
   }

   /** Comparision of fractions.
    * @param m second fraction
    * @return -1 if this < m; 0 if this==m; 1 if this > m
    */
   @Override
   public int compareTo (Lfraction m) {
		/**
		 * (a/b)?(x/y) is the same as (a*y)?(x*b)
		 */
		long temp1 = this.numerator * m.denominator;
		long temp2 = m.numerator*this.denominator;
		if(temp1==temp2){
			return 0;
		} else if (temp1 > temp2) {
			return 1;
		} else {
			return -1;
		}
   }

   /** Clone of the fraction.
    * @return new fraction equal to this
    */
   @Override
   public Object clone() throws CloneNotSupportedException {
		return new Lfraction(this.numerator, this.denominator);
   }

   /** Integer part of the (improper) fraction. 
    * @return integer part of this fraction
    */
   public long integerPart() {
		return this.numerator / this.denominator;
   }

   /** Extract fraction part of the (improper) fraction
    * (a proper fraction without the integer part).
    * @return fraction part of this fraction
    */
   public Lfraction fractionPart() {
	   long new_numerator=Math.abs(this.numerator);
	   long new_denominator=this.denominator;
	   while (new_numerator >= new_denominator) {
		   new_numerator-=new_denominator;
	   }
	   if(this.numerator<0){
		   new_numerator*=-1;
	   }
		return new Lfraction(new_numerator, new_denominator);
   }

   /** Approximate value of the fraction.
    * @return numeric value of this fraction
    */
   public double toDouble() {
		return (double) this.numerator / this.denominator;
   }

   /** Double value f presented as a fraction with denominator d > 0.
    * @param f real number
    * @param d positive denominator for the result
    * @return f as an approximate fraction of form n/d
    */
   public static Lfraction toLfraction (double f, long d) {
		long new_numerator = Math.round(f * d);
		return new Lfraction(new_numerator, d);
   }

   /** Conversion from string to the fraction. Accepts strings of form
    * that is defined by the toString method.
    * @param s string form (as produced by toString) of the fraction
    * @return fraction represented by s
    */
   public static Lfraction valueOf (String s) {
		String elements[] = s.split("/");
		try{
			if (elements[0] == null || elements[1] == null) {
				throw new RuntimeException("string " + s + " is empty or missing 1 of the elements");
			}
		}catch (Exception e) {
			throw new RuntimeException("string" + s + " missing 1 of the elements");
		}
		if (Long.parseLong(elements[1]) == 0) {
			throw new RuntimeException("string " + s + " Denominator is zero");
		}
		return new Lfraction(Long.parseLong(elements[0]), Long.parseLong(elements[1]));
   }
}


