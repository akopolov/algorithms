package home2;

public class Test2 {

	public static void main(String[] args) {
		int[] b = { 4, 3, 5, 1, 1, 7, 1, 5, 5, 5, 0, 4, 4, 0, 6, -1, 101 };
		binaryInsertionSort(b);
	}

	public static void binaryInsertionSort(int[] a) {
		// TODO!!! Your method here!
		int k = 0;
		for (int i = 1; i < a.length; i++) {
			binSort(a, i, 0, ++k);
		}
		for (int i : a) {
			System.out.print(i + " ");
		}
	}

	public static void binSort(int[] a, int i, int lower, int upper) {
		int temp = a[i];
		if (upper - lower > 1) {
			int mid = (int) Math.ceil((double) (lower + upper) / 2);
			if (a[mid] < temp) {
				binSort(a, i, mid, upper);
			} else if (a[mid] > temp) {
				binSort(a, i, lower, mid);
			} else {
				move(a, i, mid);
				a[mid] = temp;
			}
		} else {
			if (a[lower] <= temp && a[upper] > temp) {
				move(a, i, lower + 1);
				a[lower + 1] = temp;
			} else if (a[lower] > temp) {
				move(a, i, lower);
				a[lower] = temp;
			} else {
				move(a, i, upper);
				a[upper] = temp;
			}
		}
	}

	public static void move(int[] a, int i, int pos) {
		System.out.println(pos + " " + i + " " + a.length);
		for (int c : a) {
			System.out.print(c + " ");
		}
		System.out.println();
		System.arraycopy(a, pos, a, pos + 1, i - pos);
		for (int c : a) {
			System.out.print(c + " ");
		}
		System.out.println();
		// for (int i = move_from; i > move_to; i--) {
		// a[i] = a[i - 1];
		// }
	}
}
