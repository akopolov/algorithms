package home2;

public class Test {
	
	public static void main(String[] args) {
		int[] b = { 4, 3, 5, 1, 1, 7, 1, 5, 5, 5, 0, 4, 4, 0, 6, -1, 101 };
		binaryInsertionSort(b);
	}

	public static void binaryInsertionSort(int[] a) {
		// TODO!!! Your method here!
		int k = 0;
		for (int i = 1; i < a.length; i++) {
			binSort(a, i, 0, ++k);
		}
	}

	public static void binSort(int[] a, int i, int lower, int upper) {
		int temp = a[i];
		if (upper - lower > 1) {
			int mid = (int) Math.ceil((double) (lower + upper) / 2); 
			if (a[mid] < temp) {
				binSort(a,i, mid, upper); 
			} else if (a[mid] > temp) { 
				binSort(a, i, lower, mid);
			} else {
				move(a, i, mid);
				a[mid] = temp;
			}
			if (upper - lower % 2 == 0) {
				mid = (lower + upper) / 2;
				if (temp < a[mid]) {
					binSort(a, i, lower, mid);
				} else if (temp > a[mid + 1]) {
					binSort(a, i, mid + 1, upper);
				} else if (temp >= a[mid] && temp <= a[mid + 1]) {
					binSort(a, i, mid, mid + 1);
				}
			} else {
				mid = (int) Math.ceil((double) (lower + upper) / 2);
				if (temp > a[mid]) {
					binSort(a, i, mid, upper);
				} else if (temp < a[mid]) {
					binSort(a, i, lower, mid);
				} else {
					move(a, i, mid);
					a[mid] = temp;
				}
			}
		} else {
			if (a[lower] == temp) {
				move(a, i, lower + 1);
				a[lower + 1] = temp;
			} else if (a[lower] > temp) {
				move(a, i, lower);
				a[lower] = temp;
			} else if (a[lower] < temp && a[upper] > temp) {
				move(a, i, lower + 1);
				a[lower + 1] = temp;
			} else {
				move(a, i, upper);
				a[upper] = temp;
			}
		}
	}

	public static void move(int[] a, int move_from, int move_to) {
		for (int i = move_from; i > move_to; i--) {
			a[i] = a[i - 1];
		}
	}
}

/*
 * int mid = (int) Math.ceil((double) (lower + upper) / 2); if (a[mid] < temp) {
 * System.out.println(temp + " is bigger than " + a[mid] + " mid"); binSort(a,
 * i, mid, upper); } else if (a[mid] > temp) { System.out.println(temp +
 * " is smaller than " + a[mid] + " mid"); binSort(a, i, lower, mid); } else {
 * System.out.println(a[mid] + " equals " + temp); move(a, i, mid); a[mid] =
 * temp; }
 */
